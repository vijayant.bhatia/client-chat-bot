package com.cs.apac.hackathon.client.chatbot.controller;

import com.cs.apac.hackathon.client.chatbot.dto.*;
import com.cs.apac.hackathon.client.chatbot.entity.SenderSession;
import com.cs.apac.hackathon.client.chatbot.service.AccountService;
import com.cs.apac.hackathon.client.chatbot.service.SenderSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private SenderSessionService senderSessionService;

    @GetMapping(value = "/details/{ASID}")
    public ResponseEntity getAccountDetails(@PathVariable String ASID) {
        Account account = accountService.getAccount(ASID);
        return ResponseEntity.ok(account);
    }

    @GetMapping(value = "/balance/{socialId}")
    public ResponseEntity getAccountBalance(@PathVariable String socialId) {
        Account account = accountService.getAccount(socialId);
        String balance = "null";
        if (null != account) {
            balance = account.getAccountBalance().toString();
        }
        return ResponseEntity.ok(balance);
    }

    @GetMapping(value = "/transactions/last/{socialId}")
    public ResponseEntity getLast5Txn(@PathVariable String socialId) {
        List<Transaction> transactionDetails = accountService.getTransactions(socialId);
        List<Transaction> transactionList = new ArrayList<>();
        if(null == transactionDetails) {
            return ResponseEntity.ok(transactionDetails);
        }
        if (!transactionDetails.isEmpty()) {
            int transactionCount = 0;
            if(transactionDetails.size()<6) {
                return ResponseEntity.ok(transactionDetails);
            }
            while (transactionCount < 5) {
                transactionList.add(transactionDetails.get(transactionCount));
                transactionCount++;
            }
        }
        return ResponseEntity.ok(transactionList);
    }

    @GetMapping(value = "/branch/{ASID}")
    public ResponseEntity getBranchDetails(@PathVariable String ASID) {
        Account account = accountService.getAccount(ASID);
        Branch branch = null;
        if (null != account) {
            branch = account.getBranch();
        }
        return ResponseEntity.ok(branch);
    }

    @CrossOrigin(origins = "*", allowedHeaders = {"Origin", "Content-Type", "X-Auth-Token", "content-type"},
            methods = {GET, POST, PUT, PATCH, DELETE, OPTIONS})
    @PostMapping(value = "/link")
    public ResponseEntity linkFbToBank(@RequestBody FbLinkRequest fbLinkRequest) {
        SenderSession senderSession = new SenderSession();
        senderSession.setAuthenticatedFlag(false);
        senderSession.setSecurePin(String.valueOf(fbLinkRequest.getSecurePin()));
        senderSession.setSenderID(fbLinkRequest.getASID());
        senderSession.setLastLoginTS(new Timestamp(new Date().getTime()));
        senderSession.setUpdateTS(new Timestamp(new Date().getTime()));
        senderSessionService.saveSenderSession(senderSession);
        return ResponseEntity.ok(null);
    }
}