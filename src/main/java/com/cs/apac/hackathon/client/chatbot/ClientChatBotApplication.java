package com.cs.apac.hackathon.client.chatbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientChatBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientChatBotApplication.class, args);
	}
}
