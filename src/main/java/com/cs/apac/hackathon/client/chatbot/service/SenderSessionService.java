package com.cs.apac.hackathon.client.chatbot.service;

import com.cs.apac.hackathon.client.chatbot.entity.SenderSession;
import com.cs.apac.hackathon.client.chatbot.repository.SenderSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SenderSessionService {

    @Autowired
    SenderSessionRepository repository;

    public SenderSession getSenderSession(String senderId) {
        return repository.findById(senderId).orElse(null);
    }

    public SenderSession saveSenderSession(SenderSession senderSession) {
        return repository.save(senderSession);
    }
}
