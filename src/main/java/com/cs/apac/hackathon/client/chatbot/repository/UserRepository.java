package com.cs.apac.hackathon.client.chatbot.repository;

import com.cs.apac.hackathon.client.chatbot.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
}