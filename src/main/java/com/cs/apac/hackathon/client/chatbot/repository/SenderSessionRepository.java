package com.cs.apac.hackathon.client.chatbot.repository;

import com.cs.apac.hackathon.client.chatbot.entity.SenderSession;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SenderSessionRepository extends CrudRepository<SenderSession, String>{

}
