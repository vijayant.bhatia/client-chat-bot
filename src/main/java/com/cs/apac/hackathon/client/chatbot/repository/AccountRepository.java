package com.cs.apac.hackathon.client.chatbot.repository;

import com.cs.apac.hackathon.client.chatbot.entity.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<Account, String> {
    List<Account> findBySocialId(@Param("socialId")String socialId);
}