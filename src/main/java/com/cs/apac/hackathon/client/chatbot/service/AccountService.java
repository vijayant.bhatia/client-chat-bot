package com.cs.apac.hackathon.client.chatbot.service;

import com.cs.apac.hackathon.client.chatbot.dto.Account;
import com.cs.apac.hackathon.client.chatbot.dto.AccountsResponse;
import com.cs.apac.hackathon.client.chatbot.dto.Transaction;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service
public class AccountService {

    public Account getAccount(String ASID) {

        try (InputStream inJson = getClass().getResourceAsStream("/dummyResponse/transactionData.json")) {
            AccountsResponse accountsResponse = new ObjectMapper().readValue(inJson, AccountsResponse.class);
            return accountsResponse.getAccounts().stream().filter(account -> ASID.equals(account.getASID()))
                    .findFirst().orElse(null);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException("test");
        }
    }

    public List<Transaction> getTransactions(String ASID) {
        try (InputStream inJson = getClass().getResourceAsStream("/dummyResponse/transactionData.json")) {
            AccountsResponse accountsResponse = new ObjectMapper().readValue(inJson, AccountsResponse.class);
            Account account1 = accountsResponse.getAccounts().stream().filter(account -> ASID.equals(account.getASID()))
                    .findFirst().orElse(null);
            if (null == account1) {
                return null;
            }
            return account1.getTransactions();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException("test");
        }
    }

}
