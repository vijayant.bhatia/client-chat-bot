package com.cs.apac.hackathon.client.chatbot.service;

import com.cs.apac.hackathon.client.chatbot.entity.User;
import com.cs.apac.hackathon.client.chatbot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User getUser(String userId){
        return userRepository.findById(userId).orElse(null);
    }
}